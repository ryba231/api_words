package api_words;

public class WordsDto {
    private int nr;
    private String word;
    private String first;
    private String second;

    public WordsDto(int nr, String word, String first, String second){

        this.nr = nr;
        this.word = word;
        this.first = first;
        this.second = second;
    }

    public WordsDto(int nr){

        this.nr = nr;
    }


    public String toString(String separator) {
        return getNr() + separator + getWord() + separator +
                getFirst() + separator + getSecond();

    }

    public int getNr() {
        return nr;
    }

    public String getWord() {
        return word;
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setSecond(String second) {
        this.second = second;
    }
}
