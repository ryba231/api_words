package api_words;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class words {
    private JPanel newRootPanel;
    private JPanel textPanel;
    private JPanel buttonPanel;
    private JButton randomButton;
    private JButton checkButton;
    private JTextField randomWordsTextField;
    private JTextField firstTextField;
    private JTextField secondTextField;
    private JButton clearButton;
    private JTextField pathTextField;
    private JButton pathButton;
    static ArrayList<WordsDto> lista = new ArrayList<>();
    static String path;
    static String header;
    static int random;
    static Random r = new Random();
    static String first;
    static String second;


    public words() {
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                randomWordsTextField.setText(" ");
                firstTextField.setText("");
                secondTextField.setText("");
                firstTextField.setBackground(Color.white);
                secondTextField.setBackground(Color.white);

            }
        });
        randomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            path = pathTextField.getText();
                if(lista.isEmpty()){
                    lista = czytajPlik();
                }

                random = r.nextInt(108)+1;

               for (WordsDto wordsDto : lista){
                   if(wordsDto.getNr() == random){

                       randomWordsTextField.setText(wordsDto.getWord());
                        first = wordsDto.getFirst();
                        second = wordsDto.getSecond();
                   }
               }
               firstTextField.setText("");
                secondTextField.setText("");
                firstTextField.setBackground(Color.white);
                secondTextField.setBackground(Color.white);

            }

        });
        pathButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                pathTextField.setText("C:\\Users\\Marek\\IdeaProjects\\api_words\\main\\src\\java\\api_words\\word.csv");
            }
        });
        checkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            if(firstTextField.getText().equals(first) && secondTextField.getText().equals(second)){
                firstTextField.setBackground(Color.green);
                secondTextField.setBackground(Color.green);
            }else {
                firstTextField.setBackground(Color.red);
                secondTextField.setBackground(Color.red);
            }
            System.out.println(first);
            System.out.println(firstTextField.getText());
            System.out.println(second);
            System.out.println(secondTextField.getText());


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("words");
        frame.setContentPane(new words().newRootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    private static ArrayList<WordsDto> czytajPlik() {
        Path sciezkaDoPliku = Paths.get(path);
        ArrayList<String> odczyt = new ArrayList<>();
        try {
            odczyt = (ArrayList) Files.readAllLines(sciezkaDoPliku);

        }catch (IOException ex){
            System.out.println("Brak pliku!");
        }
        header = odczyt.get(0);
        odczyt.remove(0);
        ArrayList<WordsDto> wordsDtos = doObiektow(odczyt);
        return wordsDtos;
    }

    private static ArrayList<WordsDto> doObiektow(ArrayList<String> odczyt) {
   ArrayList<WordsDto> wordsDtos = new ArrayList<>();
   for (String linia : odczyt){
       String[] l = linia.split(",");
       WordsDto wordsDto = new WordsDto(
               Integer.parseInt(l[0]),
               l[1],
               l[2],
               l[3]
               );
       wordsDtos.add(wordsDto);
   }
        return wordsDtos;
    }
    private static void wypisz(){
        for (WordsDto wordsDto : lista){
            System.out.println(wordsDto.toString(","));
        }
    }

}
